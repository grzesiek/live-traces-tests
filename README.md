# Performance tests for cloud native build logs

This project is being used to generate a significant amount of jobs using live-traces feature and to measure their impact on production environment.

See:

* Feature rollout issue: https://gitlab.com/gitlab-org/gitlab/-/issues/217988
* Production change issue: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/2497
